package main

import (
	"log"

	"dndapi/config"
	"dndapi/core/database"
	"dndapi/core/router"
)

func init() {
	config.Setup()
	database.Setup()
}

func main() {
	configuration := config.GetConfig()

	r := router.Setup()
	if err := r.Run("127.0.0.1:" + configuration.Server.Port); err != nil {
		log.Fatalln(err)
		return
	}
}