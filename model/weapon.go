package model

import (
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type Weapon struct {
	ID uuid.UUID `json:"id" gorm:"type:varchar(36);not null;primary_key"`
	Name string `json:"name" gorm:"type:varchar(255);not null"`
	Regular uint `json:"regular" gorm:"not null"`
	Hard uint `json:"hard" gorm:"not null"`
	Extreme uint `json:"extreme" gorm:"not null"`
	Damage uint `json:"damage" gorm:"not null"`
	Range uint `json:"range" gorm:"not null"`
	Attacks uint `json:"attacks" gorm:"not null"`
	Ammo uint `json:"ammo" gorm:"not null"`
	Malfunction uint `json:"malfunction" gorm:"not null"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

func (weapon *Weapon) BeforeCreate(scope *gorm.Scope) error {
	if err := scope.SetColumn("ID", uuid.New()); err != nil {
		log.Println("Error inserting Character into the database - cannot add Uuid")
	}
	return nil
}
