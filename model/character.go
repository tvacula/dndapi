package model

import (
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type Character struct {
	ID uuid.UUID `json:"id" gorm:"type:varchar(36);not null;primary_key"`
	Name string `json:"name" gorm:"type:varchar(255);not null"`
	Player string `json:"player" gorm:"type:varchar(255);not null"`
	Occupation string `json:"occupation" gorm:"type:varchar(255);not null"`
	Age uint `json:"age" gorm:"not null"`
	Sex string `json:"sex" gorm:"not null"`
	Residence string `json:"residence" gorm:"type:varchar(255);not null"`
	Birthplace string `json:"birthplace" gorm:"type:varchar(255);not null"`
	PersonalDescription string `json:"personalDescription" gorm:"type:text;not null"`
	IdeologyBeliefs string `json:"ideologyBeliefs" gorm:"type:text;not null"`
	SignificantPeople string `json:"significantPeople" gorm:"type:text;not null"`
	MeaningfulLocations string `json:"meaningfulLocations" gorm:"type:text;not null"`
	TreasuredPossessions string `json:"treasuredPossessions" gorm:"type:text;not null"`
	Traits string `json:"traits" gorm:"type:text;not null"`
	InjuriesScars string `json:"injuriesScars" gorm:"type:text;not null"`
	PhobiasManias string `json:"phobiasManias" gorm:"type:text;not null"`
	ArcaneTomesSpellsArtifacts string `json:"arcaneTomesSpellsArtifacts" gorm:"type:text;not null"`
	EncountersWithStrangeEntities string `json:"encountersWithStrangeEntities" gorm:"type:text;not null"`
	SpendingLevel uint `json:"spendingLevel" gorm:"not null"`
	Cash uint `json:"cash" gorm:"not null"`
	MoveRate uint `json:"moveRate" gorm:"not null"`
	HitPoints uint `json:"hitPoints" gorm:"not null"`
	MaxHitPoints uint `json:"maxHitPoints" gorm:"default:20;not null"`
	Sanity uint `json:"sanity" gorm:"not null"`
	MaxSanity uint `json:"maxSanity" gorm:"default:99;not null"`
	Luck uint `json:"luck" gorm:"not null"`
	MaxLuck uint `json:"maxLuck" gorm:"default:99;not null"`
	Magic uint `json:"magic" gorm:"not null"`
	MaxMagic uint `json:"maxMagic" gorm:"default:24;not null"`
	MajorWound bool `json:"majorWound" gorm:"default:false;not null"`
	Dying bool `json:"dying" gorm:"default:false;not null"`
	Unconscious bool `json:"unconscious" gorm:"default:false;not null"`
	Dead bool `json:"dead" gorm:"default:false;not null"`
	TemporarilyInsane bool `json:"temporarilyInsane" gorm:"default:false;not null"`
	IndefinitelyInsane bool `json:"indefinitelyInsane" gorm:"default:false;not null"`
	DamageBonus uint `json:"damageBonus" gorm:"not null"`
	Build uint `json:"build" gorm:"not null"`
	Stat Stat `json:"stats" gorm:"embedded;embedded_prefix:stat_"`
	Skill Skill `json:"skills" gorm:"embedded;embedded_prefix:skill_"`
	Weapon []Weapon `json:"weapons" gorm:"many2many:character_weapons"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

func (character *Character) BeforeCreate(scope *gorm.Scope) error {
	if err := scope.SetColumn("ID", uuid.New()); err != nil {
		log.Println("Error inserting Character into the database - cannot add Uuid")
	}
	return nil
}

type Stat struct {
	Strength uint `json:"strength" gorm:"not null"`
	Constitution uint `json:"constitution" gorm:"not null"`
	Size uint `json:"size" gorm:"not null"`
	Dexterity uint `json:"dexterity" gorm:"not null"`
	Appearance uint `json:"appearance" gorm:"not null"`
	Intelligence uint `json:"intelligence" gorm:"not null"`
	Power uint `json:"power" gorm:"not null"`
	Education uint `json:"education" gorm:"not null"`
}

type Skill struct {
	Accounting int `json:"accounting" gorm:"not null"`
	Anthropology int `json:"anthropology" gorm:"not null"`
	Appraise int `json:"appraise" gorm:"not null"`
	Archaeology int `json:"archeology" gorm:"not null"`
	ArtCraft int `json:"artCraft" gorm:"not null"`
	Charm int `json:"charm" gorm:"not null"`
	Climb int `json:"climb" gorm:"not null"`
	CreditRating int `json:"creditRating" gorm:"not null"`
	CthulhuMythos int `json:"cthulhuMythos" gorm:"not null"`
	Disguise int `json:"disguise" gorm:"not null"`
	Dodge int `json:"dodge" gorm:"not null"`
	DriveAuto int `json:"driveAuto" gorm:"not null"`
	RepairElectronics int `json:"repairElectronics" gorm:"not null"`
	RepairMechanics int `json:"repairMechanics" gorm:"not null"`
	FastTalk int `json:"fastTalk" gorm:"not null"`
	FightingBrawl int `json:"fightingBrawl" gorm:"not null"`
	FirearmsHandgun int `json:"firearmsHandgun" gorm:"not null"`
	FirearmsRifleShotgun int `json:"firearmsRifleShotgun" gorm:"not null"`
	FirstAid int `json:"firstAid" gorm:"not null"`
	History int `json:"history" gorm:"not null"`
	Intimidate int `json:"intimidate" gorm:"not null"`
	Jump int `json:"jump" gorm:"not null"`
	LanguageOther int `json:"languageOther" gorm:"not null"`
	LanguageOwn int `json:"languageOwn" gorm:"not null"`
	Law int `json:"law" gorm:"not null"`
	LibraryUse int `json:"libraryUse" gorm:"not null"`
	Listen int `json:"listen" gorm:"not null"`
	Locksmith int `json:"locksmith" gorm:"not null"`
	Medicine int `json:"medicine" gorm:"not null"`
	NaturalWorld int `json:"naturalWorld" gorm:"not null"`
	Navigate int `json:"navigate" gorm:"not null"`
	Occult int `json:"occult" gorm:"not null"`
	HeavyMachineOperation int `json:"heavyMachineOperation" gorm:"not null"`
	Persuade int `json:"persuade" gorm:"not null"`
	Pilot int `json:"pilot" gorm:"not null"`
	Psychology int `json:"psychology" gorm:"not null"`
	Psychoanalysis int `json:"psychoanalysis" gorm:"not null"`
	Ride int `json:"ride" gorm:"not null"`
	Science int `json:"science" gorm:"not null"`
	SleightOfHand int `json:"sleightOfHand" gorm:"not null"`
	SpotHidden int `json:"spotHidden" gorm:"not null"`
	Stealth int `json:"stealth" gorm:"not null"`
	Survival int `json:"survival" gorm:"not null"`
	Swim int `json:"swim" gorm:"not null"`
	Throw int `json:"throw" gorm:"not null"`
	Track int `json:"track" gorm:"not null"`
}