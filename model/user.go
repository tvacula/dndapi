package model

import (
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type User struct {
	ID uuid.UUID `json:"id" gorm:"type:varchar(36);not null;primary_key"`
	Username string `json:"username" gorm:"varchar(100);not null"`
	Password string `json:"password" gorm:"type:varchar(255);not null"`
	Email string `json:"email" gorm:"type:varchar(255);not null"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

func (user *User) BeforeCreate(scope *gorm.Scope) error {
	if err := scope.SetColumn("ID", uuid.New()); err != nil {
		log.Println("Error inserting User into the database - cannot add Uuid")
	}
	return nil
}
