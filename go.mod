module dndapi

go 1.14

require (
	github.com/appleboy/gin-jwt v2.5.0+incompatible
	github.com/appleboy/gin-jwt/v2 v2.6.3
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.14
	github.com/spf13/viper v1.7.0
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0 // indirect
)
