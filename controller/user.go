package controller

import (
	"log"
	"net/http"

	"dndapi/core/database"
	"dndapi/model"

	"github.com/gin-gonic/gin"
)

func CreateUser(ctx *gin.Context) {
	db = database.GetDB()
	var user User
	var err error

	if err := ctx.BindJSON(&user); err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	user.Password, err = model.HashPassword(user.Password)

	if err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	if err := db.Create(&user).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, user)
}