package controller

import (
	"dndapi/model"

	"github.com/jinzhu/gorm"
)

var db *gorm.DB

type Character = model.Character
type Weapon = model.Weapon
type User = model.User
