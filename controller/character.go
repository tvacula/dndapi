package controller

import (
	"log"
	"net/http"

	"dndapi/core/database"

	"github.com/gin-gonic/gin"
)

func GetCharacterById(ctx *gin.Context) {
	id := ctx.Params.ByName("id")

	db = database.GetDB()
	var character Character
	var weapons []Weapon

	if err := db.Where("id = ?", id).First(&character).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	db.Model(&character).Related(&weapons, "Weapon")
	character.Weapon = weapons

	ctx.JSON(http.StatusOK, character)
}

func GetCharacters(ctx *gin.Context) {
	db = database.GetDB()
	var characters []Character

	if err := db.Find(&characters).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, characters)
}

func CreateCharacter(ctx *gin.Context) {
	db = database.GetDB()
	var character Character

	if err := ctx.BindJSON(&character); err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	if err := db.Create(&character).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, character)
}

func UpdateCharacterById(ctx *gin.Context) {
	id := ctx.Params.ByName("id")

	db = database.GetDB()
	var character Character

	if err := db.Where("id = ?", id).First(&character).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	if err := ctx.BindJSON(&character); err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	db.Save(&character)
	ctx.JSON(http.StatusOK, character)
}

func DeleteCharacterById(ctx *gin.Context) {
	id := ctx.Params.ByName("id")

	db = database.GetDB()
	var character Character

	if err := db.Where("id = ?", id).Delete(&character).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"id#" + id: "deleted"})
}
