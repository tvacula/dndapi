package controller

import (
	"log"
	"net/http"

	"dndapi/core/database"

	"github.com/gin-gonic/gin"
)

func GetWeaponById(ctx *gin.Context) {
	id := ctx.Params.ByName("id")

	db = database.GetDB()
	var weapon Weapon

	if err := db.Where("id = ?", id).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, weapon)
}

func GetWeapons(ctx *gin.Context) {
	db = database.GetDB()
	var weapons []Weapon

	if err := db.Find(&weapons).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, weapons)
}

func CreateWeapon(ctx *gin.Context) {
	db := database.GetDB()
	var weapon Weapon

	if err := ctx.BindJSON(&weapon); err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	if err := db.Create(&weapon).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, weapon)
}

func UpdateWeaponById(ctx *gin.Context) {
	id := ctx.Params.ByName("id")
	db = database.GetDB()
	var weapon Weapon

	if err := db.Where("id = ?", id).First(&weapon).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	if err := ctx.BindJSON(&weapon); err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	db.Save(&weapon)
	ctx.JSON(http.StatusOK, weapon)
}

func DeleteWeaponById(ctx *gin.Context) {
	id := ctx.Params.ByName("id")

	db = database.GetDB()
	var weapon Weapon

	if err := db.Where("id = ?", id).Delete(&weapon).Error; err != nil {
		log.Println(err)
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}
}