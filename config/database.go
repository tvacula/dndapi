package config

type DatabaseConfiguration struct {
	Database string
	Username string
	Password string
	Host 	 string
	Port 	 string
}