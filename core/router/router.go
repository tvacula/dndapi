package router

import (
	"dndapi/controller"
	"dndapi/core/middleware"

	"github.com/gin-gonic/gin"
)

func Setup() *gin.Engine {
	router := gin.Default()

	router.Use(middleware.CORS())

	characters := router.Group("/characters")
	{
		characters.GET("/", controller.GetCharacters)
		characters.POST("/", controller.CreateCharacter)
		characters.GET("/:id", controller.GetCharacterById)
		characters.DELETE("/:id", controller.DeleteCharacterById)
		characters.PUT("/:id", controller.UpdateCharacterById)
	}

	weapons := router.Group("/weapons")
	{
		weapons.GET("/", controller.GetWeapons)
		weapons.POST("/", controller.CreateWeapon)
		weapons.GET("/:id", controller.GetWeaponById)
		weapons.DELETE("/:id", controller.DeleteWeaponById)
		weapons.PUT("/:id", controller.UpdateWeaponById)
	}

	auth := router.Group("/auth")
	{
		auth.GET("/login", middleware.JwtAuthentication().LoginHandler)
		auth.POST("/register", controller.CreateUser)
		auth.GET("/refresh", middleware.JwtAuthentication().RefreshHandler)
	}

	return router
}