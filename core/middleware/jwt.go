package middleware

import (
	"github.com/google/uuid"
	"log"
	"time"

	"dndapi/core/database"
	"dndapi/model"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type User = model.User

var db *gorm.DB
var identityKey = "id"

type login struct {
	Email string `form:"email" json:"email" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type JwtUser struct {
	ID uuid.UUID
	Username string
	Email string
}

func JwtAuthentication() *jwt.GinJWTMiddleware {
	db = database.GetDB()

	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "test zone",
		Key:         []byte("s4opupr+kAS=wOpus6is"),
		Timeout:     time.Hour,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*JwtUser); ok {
				return jwt.MapClaims{
					identityKey: v.ID.String(),
				}
			}
			return jwt.MapClaims{}
		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &User{
				ID: uuid.MustParse(claims[identityKey].(string)),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginValues login
			var user User
			if err := c.ShouldBind(&loginValues); err != nil {
				return nil, jwt.ErrMissingLoginValues
			}

			if err := db.Where("email = ?", loginValues.Email).First(&user).Error; err != nil {
				log.Println(err)
				return nil, jwt.ErrFailedAuthentication
			}

			if passwordMatches := model.CheckPasswordHash(loginValues.Password, user.Password); passwordMatches == true {
				return &JwtUser{
					ID: user.ID,
					Username: user.Username,
					Email: user.Email,
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		TokenLookup: "header: Authorization",
		TokenHeadName: "Bearer",
		TimeFunc: time.Now,
	})

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}

	return authMiddleware
}
