package database

import (
	"fmt"

	"dndapi/config"
	"dndapi/model"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var (
	DB *gorm.DB
	err error
	DBErr error
)

type Database struct {
	*gorm.DB
}

func Setup() {
	var db = DB

	configuration := config.GetConfig()

	database := configuration.Database.Database
	username := configuration.Database.Username
	password := configuration.Database.Password
	host := configuration.Database.Host
	port := configuration.Database.Port

	dbString := username + ":" + password + "@(" + host + ":" + port + ")/" + database + "?charset=utf8&parseTime=True&loc=Local"
	db, err = gorm.Open("mysql", dbString)
	if err != nil {
		DBErr = err
		fmt.Println("Database error: ", err)
	}

	db.LogMode(false)

	db.AutoMigrate(&model.Character{}, &model.Weapon{}, &model.User{})

	DB = db
}

func GetDB() *gorm.DB {
	return DB
}

func getDBErr() error {
	return DBErr
}